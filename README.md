# ftp_win_sweepListAllFiles


    ruby ftp_win_sweepListAllFiles.rb my.ftp.server.com myUser myPass / | tee ftp_all_files.txt


Ruby script to list all files/dirs from a microsoftp ftpd server

Created while exploring hackthebox.eu Netmon machine

PS: With an unix ftp server would need improvements in  regexp detection of "DIR"

Heavily adapted from another github script

GPLv3